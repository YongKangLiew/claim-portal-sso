import React, {useEffect} from "react";
import Layout from '../components/shared/layout';

const SsoAuthenticationPage = () => {

  useEffect(() => {
    const queryParams = new URLSearchParams(window.location.search);
    const authServerUrl = queryParams.get('auth_server_url');
    const code = queryParams.get('code');
    const codeExpiresIn = queryParams.get('code_expires_in');
    const state = queryParams.get('state');
    const apiServerUrl = queryParams.get('api_server_url');
    console.log("authServerUrl",authServerUrl);
    console.log("code",code);
    console.log("codeExpiresIn",codeExpiresIn);
    console.log("state",state);
    console.log("apiServerUrl",apiServerUrl);
    issueToken(code).then(res => {
      console.log("token",res)
    });
  }, []);

  async function issueToken(code) {
    // let body = {
    //   "grant_type":"authorization_code",
    //   "code":code,
    //   "client_id":"ut11tsg80s",
    //   "client_secret":"2A6F095B0DCB15BAE90FC2EA36F1CF96",
    //   "redirect_uri":"http://localhost:8000/ssoAuthentication",
    // };
    // let formBody = [];
    // for (let property in body) {
    //   let encodedKey = encodeURIComponent(property);
    //   let encodedValue = encodeURIComponent(body[property]);
    //   formBody.push(encodedKey + "=" + encodedValue);
    // }
    // formBody = formBody.join("&");
    // return (await fetch('https://cors-anywhere.herokuapp.com/https://api.account.samsung.com/auth/oauth2/v2/token', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/x-www-form-urlencoded'
    //   },
    //   body: formBody,
    // })).json();

    await fetch('/search?q=proxy').then(res => {
      console.log("google",res);
    });
  }

  return (
    <div className="landing-page">
      <Layout >
        <div>
          <p>SSO Authentication Page...</p>
        </div>
      </Layout>
    </div>
  )
}

export default SsoAuthenticationPage;
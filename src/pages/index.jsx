import React, {useEffect, useState} from "react";
import LandingComponent from '../components/landing/landing.component';
import Layout from '../components/shared/layout';

const IndexPage = () => {

  return (
    <div className="landing-page">
      <Layout >
        <LandingComponent />
      </Layout>

    </div>
  )
}

export default IndexPage;
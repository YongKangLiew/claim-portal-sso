import React from 'react';

export default function Footer() {
  return (
    <>
      <div className="footer-fixed" style={{backgroundColor:"#000000"}}>
        <div className="pb-20">
          <span className="footer-tnc">Terms and Conditions</span>
          <span>Privacy Policy</span>
        </div>
        <div>
          <p>bolttech @2021 All Right Reserved</p>
        </div>
      </div>
    </>
  )
}
import React from 'react';

const Header = () => {

  return (
    <>
      <div className="header-fixed" style={{backgroundColor:"#FFFFFF"}}>
        <h3>Samsung Care+ for business</h3>
      </div>
    </>
  )
}

export default Header;
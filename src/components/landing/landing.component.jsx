import React, {Component} from 'react';

class LandingComponent extends Component {
  inputRef = {
    userId: React.createRef(),
    password: React.createRef()
  }

  state = {
    userId: '',
    password: ''
  }

  render(){
    return (
      <>
        <div className="pt-75 form-group">
          <h3 className="pb-20">Your License Keys</h3>

          <div className="dashboard-card">
            <div>
              <span className="card-title">KLEN0-34JK-34KW-W34K-QE23-03NV</span>
              <span className="card-status">Active</span>
              <button className="submit-claim-btn">Submit Claim</button>
            </div>
            <div>
              <p><strong>1 Year Warranty Extension Premium</strong></p>
            </div>
            <div>
              <p className="display-12 pb-10">Valid until 15 Nov 2022</p>
            </div>
            <div>
              <span className="pr-20"><strong>View policy details</strong></span>
              <span><strong> > </strong></span>
            </div>
          </div>
        </div>
      </>
    )
  }

  validateLogin = () => {
    return this.state.userId.length > 0 && this.state.password.length > 0
  }

  onState = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onLogin = () => {
    console.log("Login Clicked")
  }

  onLogout = () => {
    console.log("Logout Clicked")
  }

  onFindId = () => {
    console.log("FindID Clicked")
  }

  onResetPassword = () => {
    console.log("Reset Password Clicked")
  }

}

export default LandingComponent;